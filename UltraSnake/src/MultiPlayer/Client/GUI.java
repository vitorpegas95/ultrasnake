package MultiPlayer.Client;

import dumbsnake.network.message.SnakeMoved;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class GUI {
    private JTextArea textArea = new JTextArea("snake positions comming soon...");

    public GUI(){
        JFrame frame = new JFrame("Dumb Snake");
        frame.add(textArea);
        textArea.setEditable(false);
        frame.setSize(500, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void setText(String text){ textArea.setText(text); }

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 30303);
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            GUI gui = new GUI();
            while(true){
                gui.setText(((SnakeMoved)objectInputStream.readObject()).newSnakePositions);
            }

        } catch (IOException | ClassNotFoundException e) {

        }
    }

}
