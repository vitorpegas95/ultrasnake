package MultiPlayer.message;

import java.io.Serializable;

public class SnakeMoved implements Serializable{
	public final String newSnakePositions;

    public SnakeMoved(String newSnakePositions) {
        this.newSnakePositions = newSnakePositions;
    }
}
