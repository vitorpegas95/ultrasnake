package MultiPlayer.server;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * Created by HugoSousa on 10-12-2014.
 */
public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(30303);
            Board board = new Board(3);
            while(true){
                board.addObserver(new DealWithClient(serverSocket.accept()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
