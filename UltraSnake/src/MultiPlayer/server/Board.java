package MultiPlayer.server;

import java.awt.Point;
import java.util.Observable;
import java.util.Random;

public class Board extends Observable {
	private final Snake[] snakes;

	public Board(int snakeCount){
		snakes = new Snake[snakeCount];
		for (int i = 0; i < snakeCount; i++){
			snakes[i] = new Snake(i, this);
			snakes[i].getPositions().add(new Point(0, i));
			snakes[i].getPositions().add(new Point(1, i));
			snakes[i].getPositions().add(new Point(2, i));
			new Thread(snakes[i]).start();
		}
	}

	public synchronized void move(Snake snake){
		int headX = snake.getPositions().get(0).x;
		int headY = snake.getPositions().get(0).y;
		switch(new Random().nextInt(4)){
			case 0: snake.getPositions().add(0, new Point(headX - 1 , headY)); break;
			case 1: snake.getPositions().add(0, new Point(headX + 1 , headY)); break;
			case 2: snake.getPositions().add(0, new Point(headX, headY - 1)); break;
			case 3: snake.getPositions().add(0, new Point(headX , headY + 1)); break;
		}
		snake.getPositions().remove(snake.getPositions().size()-1);

		StringBuilder sb = new StringBuilder();
		for(Snake arraySnake: snakes){
			sb.append("Snake " + arraySnake.id + arraySnake.getPositions() + "\n");
		}
		setChanged();
		notifyObservers(sb.toString());
		clearChanged();
	}
}
