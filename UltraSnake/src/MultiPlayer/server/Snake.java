package MultiPlayer.server;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

public class Snake implements Runnable{
	public final int id;
	private final LinkedList<Point> positions;
	private final Board board;
	
	public Snake(int id, Board board){
		this.id = id;
		this.board = board;
		positions = new LinkedList<>();
	}

	public List<Point> getPositions(){
		return positions;
	}

	@Override
	public void run() {
		while(true){
			board.move(this);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

			}
		}
	}
	
}
