package MultiPlayer.server;

import dumbsnake.network.message.SnakeMoved;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;

public class DealWithClient implements Observer{
    ObjectOutputStream objectOutputStream;

    public DealWithClient(Socket socket) {
        try {
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            objectOutputStream.writeObject(new SnakeMoved(arg.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
