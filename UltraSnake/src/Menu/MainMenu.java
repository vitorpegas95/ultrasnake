package Menu;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import Utilidades.Debug;
import Core.SinglePlayer;
//import Multiplayer.Multiplayer;

public class MainMenu {

	private JFrame frame;
	private JPanel center;
	private JButton single, multi;
	
	public MainMenu(){
		frame = new JFrame ("UltraSnake");
		center = new JPanel();
		single = new JButton("Single-Player");
		multi = new JButton("Multiplayer");
		
		frame.setLocation(300,300);
		frame.setSize(500, 500);
		
		center.add(single);
		single.addMouseListener(new MouseListener(){

			public void mouseClicked(MouseEvent e) {
				SinglePlayer sp = new SinglePlayer();
				
				sp.RunGame();
				frame.setVisible(false);
				
			}

			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		center.add(multi);
		multi.addMouseListener(new MouseListener(){

			public void mouseClicked(MouseEvent e) {
				String name = JOptionPane.showInputDialog("Insira o seu username:");
				//new MainMultiplayer(name);
				Debug.Log("Your Name:" + name, this);
			}

			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		frame.add(center);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		new MainMenu();
	}
}

