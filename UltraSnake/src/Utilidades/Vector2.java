package Utilidades;

public class Vector2 {
	private int x;
	private int y;
	
	public Vector2(int a, int b)
	{
		setPos(a, b);
	}
	
	public static Vector2 zero()
	{
		return new Vector2(0, 0);
	}
	
	public static Vector2 random(int x, int y)
	{
		return new Vector2(x, y);
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public void setX(int v)
	{
		this.x = v;
	}
	
	public void setY(int v)
	{
		this.y = v;
	}
	
	public void setPos(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString()
	{
		return "Vector2(" + x + ", " + y + ")";	
	}
	
	public boolean equals(Vector2 v2)
	{
		if(this.getX() == v2.getX() && this.getY() == v2.getY())
			return true;
		else
			return false;		
	}
}
