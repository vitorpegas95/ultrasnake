package Utilidades;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

/* Music Player (MP3)
 * Written by Vitor P�gas
 * Don't Delete THIS COMMENT OR NOTHING WILL HAPPEN TO YOU!
 */


public class MusicPlayer extends Thread
{
	private File soundfile;
	private static final int BUFFER_SIZE = 176400;
	public boolean isPlaying;
	
	public MusicPlayer(String filename)
	{
		soundfile = new File (filename);
	}
	
	public void run()
	{
		try 
		{
			isPlaying = true;
			byte[]  buffer = new byte[BUFFER_SIZE];

			AudioInputStream in = AudioSystem.getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, AudioSystem.getAudioInputStream(soundfile));
			AudioFormat audioFormat = in.getFormat();

			SourceDataLine line;

			line = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, audioFormat));

			line.open(audioFormat);
			line.start();

			while (true) {
			  int n = in.read(buffer, 0, buffer.length);
			  if (n < 0) {
			    break;
			  }
			  line.write(buffer, 0, n);
			}
			line.drain();
			line.close();

			isPlaying = false;
		} 
		catch (LineUnavailableException e1) 
		{
			e1.printStackTrace();
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}
		catch (UnsupportedAudioFileException e1) 
		{
			e1.printStackTrace();
		}
	}
}

/*

try {
byte[]  buffer = new byte[BUFFER_SIZE];

AudioInputStream in = AudioSystem.getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, AudioSystem.getAudioInputStream(new File ("soundtrack.mp3")));
AudioFormat audioFormat = in.getFormat();

SourceDataLine line;

line = (SourceDataLine) AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, audioFormat));

line.open(audioFormat);
line.start();

while (true) {
  int n = in.read(buffer, 0, buffer.length);
  if (n < 0) {
    break;
  }
  line.write(buffer, 0, n);
}
line.drain();
line.close();
} catch (LineUnavailableException e1) {
// TODO Auto-generated catch block
e1.printStackTrace();
} catch (IOException e1) {
// TODO Auto-generated catch block
e1.printStackTrace();
} catch (UnsupportedAudioFileException e1) {
// TODO Auto-generated catch block
e1.printStackTrace();
}
*/