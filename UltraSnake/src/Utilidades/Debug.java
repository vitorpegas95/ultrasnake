package Utilidades;


public class Debug 
{
	public static final long START_TIME = System.currentTimeMillis();
	
	public static void Log(String text, Object obj)
	{
		long RUN_TIME = (System.currentTimeMillis() - START_TIME) / 1000;
		String className;
		if(obj != null)
			className = obj.toString();
		else
			className = "Null Object";
		System.out.println("( " + RUN_TIME + "s )Log: " + text + " | At " + className);
	}
	
	public static void Warning(String text, Object obj)
	{
		long RUN_TIME = (System.currentTimeMillis() - START_TIME) / 1000;
		String className;
		if(obj != null)
			className = obj.toString();
		else
			className = "Null Object";
		System.out.println("( " + RUN_TIME + "s )WARNING: " + text + " | At " + className);
	}
}
