package Core;

import Utilidades.Vector2;

public class GridCell 
{
	private Vector2 pos;
	private boolean open = true;
	
	public GridCell(Vector2 p, boolean o)
	{
		this.setPos(p);
		this.setOpen(o);
	}

	public Vector2 getPos() {
		return pos;
	}

	public void setPos(Vector2 pos) {
		this.pos = pos;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
}
