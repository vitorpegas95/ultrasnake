package Core;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.ImageIcon;

import Utilidades.Vector2;

public class Bonus 
{
	private Vector2 pos;
	private GameControl.bonusTypes type;

	private ImageIcon banana;
	private ImageIcon wall;
	private ImageIcon speed;
	private ImageIcon power;
	
	public Bonus(Vector2 p, GameControl.bonusTypes btype)
	{
		this.setPos(p);
		this.setType(btype);
		banana = new ImageIcon("Resources/banana.png");
		wall = new ImageIcon("Resources/wall.png");
		speed = new ImageIcon("Resources/speed.png");
		power = new ImageIcon("Resources/power.png");
	}
	
	public void Update()
	{
		
	}
	
	public void Draw(Graphics g)
	{
		if(type.equals(GameControl.bonusTypes.FOOD))
		{
			g.drawImage(banana.getImage(), pos.getX(), pos.getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY(), null);
		}
		else if(type.equals(GameControl.bonusTypes.WALL))
		{
			g.drawImage(wall.getImage(), pos.getX(), pos.getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY(), null);
		}
		else if(type.equals(GameControl.bonusTypes.SPEED))
		{
			g.drawImage(speed.getImage(), pos.getX(), pos.getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY(), null);
		}
		else if(type.equals(GameControl.bonusTypes.JUMP))
		{
			g.drawImage(power.getImage(), pos.getX(), pos.getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY(), null);
		}
		

    }

	public Vector2 getPos() 
	{
		return pos;
	}

	public void setPos(Vector2 pos) 
	{
		this.pos = pos;
	}

	public GameControl.bonusTypes getType() {
		return type;
	}

	public void setType(GameControl.bonusTypes type) {
		this.type = type;
	}
}
