package Core;

import java.util.ArrayList;

import Utilidades.Debug;
import Utilidades.Functions;
import Utilidades.Vector2;

public class GameControl extends Thread
{
	private static final int MAX_BONUS = 20;
	private static final int MAX_SNAKES = 2;
	private boolean SNAKE_POS[] = {false, false, false, false};
	private GUI gui;
	private GameMaster gm;
	private int fpsRate = 300;
	
	//Game Vars
	public static Vector2 blockSize = new Vector2(30, 30);
	public static enum bonusTypes {SPEED, FOOD, WALL, JUMP};
	private GridCell[][] grid;

	public int selectedSnake = -1;

	public ArrayList<Snake> snakes = new ArrayList<Snake>();
	
	public ArrayList<SnakeObservable> snakesObs = new ArrayList<SnakeObservable>();
	
	public ArrayList<Bonus> bonusList = new ArrayList<Bonus>();
	
	private ArrayList<Vector2> wallsList = new ArrayList<Vector2>();
	
	public Thread[] snakeThreadsHolder;
	
	public GameControl(GUI g, GameMaster gm)
	{
		this.gui = g;
		this.gui.setGc(this);
		this.gm = gm;
		
		Vector2 cellNum = new Vector2(this.gui.getSize().getX() / blockSize.getX(), this.gui.getSize().getY() / blockSize.getY());
		this.grid = new GridCell[cellNum.getX()][cellNum.getY()];
		
		//Populate Cells
		for(int x = 0; x < this.grid.length; x ++)
		{
			for(int y = 0; y < this.grid[0].length; y ++)
			{
				this.grid[x][y] = new GridCell(new Vector2(x * blockSize.getX(), y * blockSize.getY()), true);
			}
		}
		
		//Add random bonus to map
		for(int i = 0; i < MAX_BONUS; i++)
		{
			Vector2 bonusStart = CoordinatesToGrid(new Vector2(Functions.rand(100, this.gui.getSize().getX() - 100), Functions.rand(100, this.gui.getSize().getY() - 100)));

			if(i < 5)
				this.AddBonus(new Bonus(
						new Vector2(
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getX(), 
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getY()), 
						bonusTypes.FOOD));
			else if( i < 6)
				this.AddBonus(new Bonus(
						new Vector2(
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getX(), 
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getY()), 
						bonusTypes.SPEED));
			else if( i < 7)
				this.AddBonus(new Bonus(
						new Vector2(
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getX(), 
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getY()), 
						bonusTypes.JUMP));

			else
				this.AddBonus(new Bonus(
						new Vector2(
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getX(), 
								this.grid[bonusStart.getX()][bonusStart.getY()].getPos().getY()), 
						bonusTypes.WALL));
		}
		
		for(int i = 0; i < MAX_SNAKES; i++)
		{
			/*
			 * OLD SNAKES
			Vector2 startPos = CoordinatesToGrid(new Vector2(Functions.rand(100, this.gui.getSize().getX() - 100), Functions.rand(100, this.gui.getSize().getY() - 100)));
			int speed = 1;
			Snake s = new Snake(new Vector2(
					this.grid[startPos.getX()][startPos.getY()].getPos().getX(), 
					this.grid[startPos.getX()][startPos.getY()].getPos().getY()), speed);
			s.setGc(this);
			this.snakes.add(s);			
			*/
			
			int startRandom = Functions.rand(0, 4);
			boolean g2g = false;
			do
			{
				startRandom = Functions.rand(0, 4);
				if(SNAKE_POS[startRandom] == false)
				{
					SNAKE_POS[startRandom] = true;
					g2g = true;
				}
				
			}	while(g2g == false);
			
			Vector2 goal = Vector2.zero();
			Vector2 startPos = Vector2.zero();
			switch(startRandom)
			{
				case 0:
					//North
					goal = new Vector2(-1, grid[0].length);
					startPos = new Vector2((int)(grid.length / 2), 1);
					
				break;
				
				case 1:
					//East
					goal = new Vector2(0, -1);
					startPos = new Vector2((int)(grid.length - 2), (int)(grid[0].length / 2));
					
				break;
				
				case 2:
					//South
					goal = new Vector2(-1, 0);
					startPos = new Vector2((int)(grid.length / 2), (int)(grid[0].length - 2));
					
				break;
				
				case 3:
					//West
					goal = new Vector2(grid.length, -1);
					startPos = new Vector2(1, (int)(grid[0].length / 2));
					
				break;
			}
			
			//startPos = CoordinatesToGrid(new Vector2(Functions.rand(100, this.gui.getSize().getX() - 100), Functions.rand(100, this.gui.getSize().getY() - 100)));
			int speed = 1;
			SnakeObservable s = new SnakeObservable(new Vector2(
					this.grid[startPos.getX()][startPos.getY()].getPos().getX(), 
					this.grid[startPos.getX()][startPos.getY()].getPos().getY()), 
					speed, 
					gm, 
					goal);
			s.setName("Snake #"+i);
			s.setGc(this);
			this.snakesObs.add(s);
		}		
	}
	
	public void run()
	{
		
		
		while(true)
		{
			//Debug.Log("Running Game Control", this.getClass());
			
			//Update Game
			Update();
			
			try 
			{
				sleep(fpsRate);
			} 
			catch (InterruptedException e) 
			{
				Debug.Log(">>>Um erro aconteceu<<<", this.getClass());
				e.printStackTrace();
			}
		}
	}
	
	public void AddBonus(Bonus b)
	{
		//Check the bonus type, to see if the snake can get pass through or not.
		bonusTypes bType = b.getType();
		//Block = true means if the cell will be closed or open.
		boolean block = false;
		switch(bType)
		{
			case SPEED:
				block = false;
			break;
			
			case FOOD:
				block = false;
			break;
			
			case WALL:
				block = false;
				this.wallsList.add(b.getPos());
			break;	
			
			case JUMP:
				block = false;
			break;
			
			default:
				block = false;
			break;
		}
		
		this.bonusList.add(b);
		this.grid[CoordinatesToGrid(b.getPos()).getX()][CoordinatesToGrid(b.getPos()).getY()].setOpen(!block);
	}
	
	public boolean isWall(Vector2 v)
	{
		for(int i = 0; i < this.wallsList.size(); i++)
		{
			if(this.wallsList.get(i).equals(v))
				return true;
		}
		return false;
	}
	
	public synchronized boolean isCellOpen(Vector2 v)
	{
		return grid[v.getX()][v.getY()].isOpen();
	}
	
	public synchronized void OpenCell(Vector2 v)
	{
		grid[v.getX()][v.getY()].setOpen(true);
		notifyAll();
	}
	
	public synchronized void CloseCell(Vector2 v)
	{
		while(!grid[v.getX()][v.getY()].isOpen())
		{
			try {
					wait();
			} catch (InterruptedException e) {
					Debug.Log("Erro no Wait..", this);
			}
		}
		
		grid[v.getX()][v.getY()].setOpen(false);
		notifyAll();
	}
	
	public static Vector2 CoordinatesToGrid(Vector2 pos)
	{
		return new Vector2(pos.getX() / blockSize.getX(), pos.getY() / blockSize.getY());
	}
	
	public void Update()
	{
		CollisionUpdate();
	}
	
	public void CollisionUpdate()
	{
		for(int i = 0; i < bonusList.size(); i++)
		{
			Bonus b = bonusList.get(i);
			
			for(int j = 0; j < snakesObs.size(); j++)
			{
				if(b.getPos().equals(snakesObs.get(j).getBody().get(0)) && !(b.getType().equals(bonusTypes.WALL)))
				{
					snakesObs.get(j).addBonus(b);
					bonusList.remove(i);
				}
			}				
		}
	}
	
	public void SnakeWin(String name)
	{
		for(int i = 0; i < snakesObs.size(); i++)
		{
			if(snakesObs.get(i).getName().equals(name))
			{
				snakesObs.get(i).setWon(true);
			}
			else
			{
				snakesObs.get(i).setLost(true);
			}
			
			this.snakeThreadsHolder[i].interrupt();
		}
	}

	public GridCell[][] getGrid() {
		return grid;
	}

	public int getFpsRate() {
		return fpsRate;
	}

	public void setFpsRate(int fpsRate) {
		this.fpsRate = fpsRate;
	}
}
