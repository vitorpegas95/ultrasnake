package Core;

import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;

import Utilidades.Debug;
import Utilidades.Functions;
import Utilidades.Vector2;

public class SnakeObservable extends Observable implements Runnable
{
	private ArrayList<Vector2> body = new ArrayList<Vector2>();
	private ArrayList<Vector2> previous = new ArrayList<Vector2>();
	private int lastDir;
	private int speed;
	private Vector2 goal;
	private boolean start = true;
	private boolean reachedGoal = false;
	
	private boolean won = false;
	private boolean lost = false;
	
	private String name = "";
	
	private int auto = 1;
	private Vector2 waypoint;
	private int jumps = 0;
	
	private GameControl gc;

	private ImageIcon headNormal = new ImageIcon("Resources/head3.png");
	private ImageIcon headWin = new ImageIcon("Resources/head.png");
	private ImageIcon headLose = new ImageIcon("Resources/head2.png");
	
	public SnakeObservable(Vector2 p, int s, Observer obs, Vector2 g)
	{
		this.addObserver(obs);
		this.speed = s;
		this.body.add(new Vector2(p.getX(), p.getY()));
		this.previous.add(body.get(0));
		
		for(int i = 1; i < 6; i++)
		{
			body.add(previous.get(i-1));
			previous.add(previous.get(i-1));
		}
		
		this.goal = g;

		CallObserver();
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setName(String name)
	{
		this.name = name;
		CallObserver();
	}
	
	public void addBonus(Bonus b)
	{
		GameControl.bonusTypes bType = b.getType();
		switch(bType)
		{
			case SPEED:
				this.speed *= 3;
				CallObserver();
			break;
			
			case FOOD:
				body.add(previous.get(previous.size()-1));
				previous.add(previous.get(previous.size()-1));
				CallObserver();
			break;
			
			case JUMP:
				jumps++;
				CallObserver();
			break;
			
			default:
				//Nope
			break;
		}
	}
	
	public void CallObserver()
	{
		setChanged();
		notifyObservers();
	}
	
	public void MoveTo(Vector2 newpos)
	{
		auto = 2;
		waypoint = newpos;
		CallObserver();
	}
	
	public void run()
	{
		while(true && !Thread.interrupted())
		{
			if(lost == false)
				Update();
			
			try 
			{
				Thread.sleep(gc.getFpsRate()/speed);
				//sleep(gc.getFpsRate());
			} 
			catch (InterruptedException e) 
			{
				
			}
		}
	}

	public void Update()
	{
		Vector2 vel = Vector2.zero();
		
		/*
		 * AUTO STATES
		 * 0 - Random Movement
		 * 1 - Move to Goal
		 * 2 - Move to User
		 */
		
		if(auto == 0)
		{
			int moveDir = Functions.rand(1, 5);
			
			do
			{
				//Calculate a new direction so that the snake doesn't return to the opposite one.
				moveDir = Functions.rand(1, 5);
			} while(moveDir == 1 && lastDir == 3 || 
					moveDir == 3 && lastDir == 1 || 
					moveDir == 2 && lastDir == 4 ||
					moveDir == 4 && lastDir == 2);
			
			switch(moveDir)
			{
				case 1:
					//North
					vel.setY(-GameControl.blockSize.getX());
				break;
				
				case 2:
					//East
					vel.setX(GameControl.blockSize.getX());
				break;
				
				case 3:
					//South
					vel.setY(GameControl.blockSize.getX());
				break;
				
				case 4:
					//West
					vel.setX(-GameControl.blockSize.getX());
				break;
			}
			
			lastDir = moveDir;
		}
		else if(auto == 1)		
		{
			//Check goal
			if(goal.getX() == -1)
			{
				//Ir para o y
				if(goal.getY() == 0)
				{
					vel.setY(-GameControl.blockSize.getY());
					lastDir = 1;
				}
				else
				{
					vel.setY(GameControl.blockSize.getY());
					lastDir = 3;
				}
			}
			else if(goal.getY() == -1)
			{
				//Ir para o x
				if(goal.getX() == 0)
				{
					vel.setX(-GameControl.blockSize.getX());
					lastDir = 4;
				}
				else
				{
					vel.setX(GameControl.blockSize.getX());
					lastDir = 2;
				}
			}
		}
		else if(auto == 2)	//User Controlled
		{
			
			if(body.get(0).getX() < waypoint.getX())
			{
				vel.setX(GameControl.blockSize.getX());
			}
			else if(body.get(0).getX() > waypoint.getX())
			{
				vel.setX(-GameControl.blockSize.getX());
			}
			else
			{
				vel.setY(0);

				if(body.get(0).getY() < waypoint.getY())
				{
					vel.setY(GameControl.blockSize.getY());
				}
				else if(body.get(0).getY() > waypoint.getY())
				{
					vel.setY(-GameControl.blockSize.getY());
				}
				else
				{
					vel.setY(0);
				}	
			}
	
			
			if(vel.equals(Vector2.zero()))
			{
				auto = 1;
				waypoint = Vector2.zero();
			}
			else
				auto = 2;
		}
		
		/*
		 * We Have the velocity now let's test position
		 */
		
		
		Vector2 testpos = new Vector2(body.get(0).getX() + vel.getX(), body.get(0).getY() + vel.getY());

		//test limits
        Vector2 testpos2 = GameControl.CoordinatesToGrid(testpos);
        if(     testpos2.getX() < 0 || testpos2.getX() > gc.getGrid().length - 1 ||     testpos2.getY() < 0 || testpos2.getY() > gc.getGrid()[0].length - 1)
        {
               
                /*
                 * Let's see if the snake is on the goal side of the board or not.
                 */
               
                Vector2 curLocation = GameControl.CoordinatesToGrid(body.get(0));
               
                if(goal.getX() == -1)
                {
                        //Ir para o y
                        //Debug.Log("Goal: " + goal.toString() + " | Current: " + curLocation.toString(), this);
                        if(curLocation.getY() == goal.getY() || testpos2.getY() == goal.getY())
                        {
                                this.setReachedGoal(true);
                                this.CallObserver();
                                
                                gc.SnakeWin(this.getName());
                        }
                        else
                        {
                                if(auto != 2)
                                        auto = 1;
                        }
                }
                else if(goal.getY() == -1)
                {
                        //Ir para o x
                        //Debug.Log("Goal: " + goal.toString() + " | Current: " + curLocation.toString(), this);
                        if(curLocation.getX() == goal.getX() || testpos2.getX() == goal.getX())
                        {
                                this.setReachedGoal(true);
                                this.CallObserver();
                               
                                gc.SnakeWin(this.getName());
                        }
                        else
                        {
                                if(auto != 2)
                                        auto = 1;
                        }
                }
        }
        else if(gc.isWall(testpos))	//FOund a wall
        {
        	//Debug.Log("Found a wall!", this);
        	auto = 0;
        }
        else if(gc.isCellOpen(testpos2))
        {                      		
        	//Debug.Log("Im walkin on sunshine yeaahhh!", this);
            for(int a = 0; a < body.size(); a++)
            {
                    Vector2 newBodyPos;
                    Vector2 curPos = body.get(a);
                    previous.set(a, curPos);
                   
                    if(a == 0)
                            newBodyPos = new Vector2(curPos.getX() + vel.getX(), curPos.getY() + vel.getY());
                    else
                            newBodyPos = new Vector2(previous.get(a-1).getX(), previous.get(a-1).getY());
                   
                    
                    //gc.setCellOpen(GameControl.CoordinatesToGrid(curPos), true);
                    gc.OpenCell(GameControl.CoordinatesToGrid(curPos));
                    //gc.setCellOpen(GameControl.CoordinatesToGrid(newBodyPos), false);
                    gc.CloseCell(GameControl.CoordinatesToGrid(newBodyPos));
                    body.set(a, newBodyPos);
            }
           
            if(auto != 2)
            	auto = 1;
        }
        else if(!gc.isCellOpen(testpos2) && jumps > 0)
        {
        	//Debug.Log("I found stuff in front of me and i have jumps. why cant i jump?", this);
        	auto = 0;
        	
        	 for(int a = 0; a < body.size(); a++)
             {
                     Vector2 newBodyPos;
                     Vector2 curPos = body.get(a);
                     previous.set(a, curPos);
                    
                     if(a == 0)
                             newBodyPos = new Vector2(curPos.getX() + vel.getX(), curPos.getY() + vel.getY());
                     else
                             newBodyPos = new Vector2(previous.get(a-1).getX(), previous.get(a-1).getY());
                    
                     
                     //gc.OpenCell(GameControl.CoordinatesToGrid(curPos));
                     //gc.CloseCell(GameControl.CoordinatesToGrid(newBodyPos));
                     
                     body.set(a, newBodyPos);
             }
        	 auto = 1;
        	 jumps --;
        }
        else
        {
                auto = 0;
        }
       
        if(gc.selectedSnake != -1)
        {
                if(!gc.snakesObs.get(gc.selectedSnake).equals(this))
                {
                	auto = 1;
                }
               
        } 
       
		CallObserver();
	}
	
	public void Draw(Graphics g)
	{        
		/*
		g.setColor(new Color(0xFE020A));
    	g.fillOval(body.get(0).getX(), body.get(0).getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY());
    	g.setColor(new Color(0xFFFFFF));
    	g.drawString("" + 0, body.get(0).getX() + (GameControl.blockSize.getX() / 2), body.get(0).getY() + (GameControl.blockSize.getY() / 2));
    	*/
		
		if(won)
			g.drawImage(headWin.getImage(), body.get(0).getX(), body.get(0).getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY(), null);
		else if(lost)
			g.drawImage(headLose.getImage(), body.get(0).getX(), body.get(0).getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY(), null);
		else
			g.drawImage(headNormal.getImage(), body.get(0).getX(), body.get(0).getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY(), null);
		
        for(int b = 1; b < body.size(); b++)
        {
        	if(won)
        		g.setColor(new Color(Functions.rand(0, 250), Functions.rand(0, 250), Functions.rand(0,250)));
        	else if(lost)
        		g.setColor(new Color(0, 0, 0));
        	else
        		g.setColor(new Color(255, 195, 170));
        	g.fillOval(body.get(b).getX() + 5, body.get(b).getY() + 5, GameControl.blockSize.getX() - 5, GameControl.blockSize.getY() - 5);
        }
        
	}

	public void setGc(GameControl gc) {
		this.gc = gc;
		//CallObserver();
	}

	public ArrayList<Vector2> getBody() {
		return body;
	}
	
	
	@Override
	public String toString()
	{
		return this.getName();
	}

	public boolean isReachedGoal() {
		return reachedGoal;
	}

	public void setReachedGoal(boolean reachedGoal) {
		this.reachedGoal = reachedGoal;
	}

	public int getJumps() {
		return jumps;
	}

	public void setJumps(int jumps) {
		this.jumps = jumps;
	}

	public boolean isWon() {
		return won;
	}

	public void setWon(boolean won) {
		this.won = won;
	}

	public boolean isLost() {
		return lost;
	}

	public void setLost(boolean lost) {
		this.lost = lost;
	}

}
