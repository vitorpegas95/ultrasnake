package Core;

import Utilidades.Debug;
import Utilidades.MusicPlayer;
import Utilidades.Vector2;


public class SinglePlayer 
{
	
	private GUI gui;
	private GameMaster gm;
	private GameControl gameController;
	private MusicPlayer mp;
	
	public SinglePlayer()
	{
		gui = new GUI(new Vector2(100, 100), new Vector2(790, 625));
		gm = new GameMaster(gui);
		gameController = new GameControl(gui, gm);
		mp = new MusicPlayer("Resources/soundtrack.mp3");
	}
	
	public void RunGame()
	{
		gui.execute();
		gameController.start();
		
		gameController.snakeThreadsHolder = new Thread[gameController.snakesObs.size()];
		
		//for(int s = 0; s < gameController.snakes.size(); s++)
		for(int s = 0; s < gameController.snakesObs.size(); s++)
		{
			//gameController.snakes.get(s).start();
			Thread t = new Thread(gameController.snakesObs.get(s));
			gameController.snakeThreadsHolder[s] = t;
			t.start();
		}
		
		mp.start();
	}
	
	public static void main(String[] args)
	{
		SinglePlayer sp = new SinglePlayer();
		
		sp.RunGame();
	}
}
