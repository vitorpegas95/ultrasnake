package Core;

import java.awt.*;
import java.util.ArrayList;

import Utilidades.Debug;
import Utilidades.Functions;
import Utilidades.Vector2;

public class Snake extends Thread
{
	private ArrayList<Vector2> body = new ArrayList<Vector2>();
	private ArrayList<Vector2> previous = new ArrayList<Vector2>();
	private int lastDir;
	private int speed;
	
	private boolean auto = true;
	private Vector2 waypoint;
	
	private GameControl gc;
	
	public Snake(Vector2 p, int s)
	{
		this.speed = s;
		body.add(new Vector2(p.getX(), p.getY()));
		previous.add(body.get(0));
		
		for(int i = 1; i < 6; i++)
		{
			body.add(previous.get(i-1));
			previous.add(previous.get(i-1));
		}
	}
	
	public void addBonus(Bonus b)
	{
		GameControl.bonusTypes bType = b.getType();
		switch(bType)
		{
			case SPEED:
				this.speed *= 3;
			break;
			
			case FOOD:
				body.add(previous.get(previous.size()-1));
				previous.add(previous.get(previous.size()-1));
			
			default:
				//Nope
			break;
		}
	}
	
	public void MoveTo(Vector2 newpos)
	{
		auto = false;
		waypoint = newpos;
	}
	
	public void run()
	{
		while(true)
		{
			Update();
			
			try 
			{
				sleep(gc.getFpsRate()/speed);
			} 
			catch (InterruptedException e) 
			{
				Debug.Log("Um erro aconteceu", this.getClass());
				e.printStackTrace();
			}
		}
	}

	public void Update()
	{

		Vector2 vel = Vector2.zero();
		
		if(auto)		//Automatic Movement (Random)
		{
			int moveDir = Functions.rand(1, 5);
			
			do
			{
				moveDir = Functions.rand(1, 5);
			} while(moveDir == 1 && lastDir == 3 || 
					moveDir == 3 && lastDir == 1 || 
					moveDir == 2 && lastDir == 4 ||
					moveDir == 4 && lastDir == 2);
			
			switch(moveDir)
			{
				case 1:
					//North
					vel.setY(-GameControl.blockSize.getX());
				break;
				
				case 2:
					//East
					vel.setX(GameControl.blockSize.getX());
				break;
				
				case 3:
					//South
					vel.setY(GameControl.blockSize.getX());
				break;
				
				case 4:
					//West
					vel.setX(-GameControl.blockSize.getX());
				break;
			}
			
			lastDir = moveDir;
		}
		else	//User Controlled
		{
			
			if(body.get(0).getX() < waypoint.getX())
			{
				vel.setX(GameControl.blockSize.getX());
			}
			else if(body.get(0).getX() > waypoint.getX())
			{
				vel.setX(-GameControl.blockSize.getX());
			}
			else
			{
				vel.setY(0);

				if(body.get(0).getY() < waypoint.getY())
				{
					vel.setY(GameControl.blockSize.getY());
				}
				else if(body.get(0).getY() > waypoint.getY())
				{
					vel.setY(-GameControl.blockSize.getY());
				}
				else
				{
					vel.setY(0);
				}	
			}
	
			
			if(vel.equals(Vector2.zero()))
			{
				auto = true;
				waypoint = Vector2.zero();
			}
		}
		Vector2 testpos = new Vector2(body.get(0).getX() + vel.getX(), body.get(0).getY() + vel.getY());
		if(gc.isCellOpen(GameControl.CoordinatesToGrid(testpos)))
		{		
			for(int a = 0; a < body.size(); a++)
	        {
				Vector2 newBodyPos;
				Vector2 curPos = body.get(a);
				previous.set(a, curPos);
				
				if(a == 0)
					newBodyPos = new Vector2(curPos.getX() + vel.getX(), curPos.getY() + vel.getY());
				else
					newBodyPos = new Vector2(previous.get(a-1).getX(), previous.get(a-1).getY());
				

				//gc.setCellOpen(GameControl.CoordinatesToGrid(curPos), true);
				//gc.setCellOpen(GameControl.CoordinatesToGrid(newBodyPos), false);
				body.set(a, newBodyPos);
	        }
		}
	}
	
	public void Draw(Graphics g)
	{        
        for(int b = 0; b < body.size(); b++)
        {
    		g.setColor(new Color(0x3E874D));
        	g.fillOval(body.get(b).getX(), body.get(b).getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY());
        	g.setColor(new Color(0xFFFFFF));
        	g.drawString("" + b, body.get(b).getX() + (GameControl.blockSize.getX() / 2), body.get(b).getY() + (GameControl.blockSize.getY() / 2));
        }
        g.setColor(Color.RED);
        g.fillRect(body.get(0).getX() + 2, body.get(0).getY() + 4, 2, 2);
        g.fillRect(body.get(0).getX() + 6, body.get(0).getY() + 4, 2, 2);
        
	}

	public void setGc(GameControl gc) {
		this.gc = gc;
	}

	public ArrayList<Vector2> getBody() {
		return body;
	}

	public void setBody(ArrayList<Vector2> body) {
		this.body = body;
	}
	
	@Override
	public String toString()
	{
		String rtn = "Size: " + body.size() + "\n";
		
		for(int i = 0; i < body.size(); i++)
		{
			rtn += "Body #" + i + " @ " + body.get(i).toString() + "\n";
		}
		return rtn;
	}
}
