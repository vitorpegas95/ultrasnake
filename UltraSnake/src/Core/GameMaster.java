package Core;

import java.util.Observable;
import java.util.Observer;

import Utilidades.Debug;


/* GameMaster Class
 * This class is the Observer Class for Ultra Snake.
 * It's goal is to be notfied for any change on the game components and work accordingly.
 * 
 */
public class GameMaster implements Observer
{

	private GUI gui;
	
	public GameMaster(GUI gui)
	{
		this.gui = gui;
	}
	
	public void update(Observable obs, Object obj) 
	{
		gui.canvas.repaint();
	}
	
}
