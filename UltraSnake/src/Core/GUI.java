package Core;

import javax.swing.*;

import Utilidades.Debug;
import Utilidades.Vector2;

import java.awt.*;
import java.util.ArrayList;
import java.awt.event.*;

public class GUI 
{
	private JFrame window;
	private Container container;
	private Vector2 pos, size;
	private GameControl gc;
	
	public Canvas canvas;
	
	private boolean firstDraw = false;
	
	public GUI(Vector2 pos, Vector2 size)
	{
		this.pos = pos;
		this.size = size;
		window = new JFrame("UltraSnake v2");
		window.setBounds(pos.getX(), pos.getY(), size.getX(), size.getY());
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.addKeyListener(new KeyListener()
		{

			public void keyPressed(KeyEvent arg0) 
			{
				
			}

			public void keyReleased(KeyEvent arg0) 
			{
				
			}

			public void keyTyped(KeyEvent arg0) 
			{
				
			}
			
		});
		setContainer(window.getContentPane());
		
		canvas = new Canvas();
		this.getWindow().add(canvas);
	}

	public void execute()
	{
		window.setVisible(true);
	}

	public JFrame getWindow() {
		return window;
	}

	public void setWindow(JFrame window) {
		this.window = window;
	}

	public Vector2 getPos() {
		return pos;
	}

	public void setPos(Vector2 pos) {
		this.pos = pos;
	}

	public Vector2 getSize() {
		return size;
	}

	public void setSize(Vector2 size) {
		this.size = size;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public void setGc(GameControl gc) {
		this.gc = gc;
	}
	
	
	class Canvas extends JPanel 
	{

		private static final long serialVersionUID = 1L;
		
		
		public Canvas() 
		{
	        setBorder(BorderFactory.createLineBorder(Color.black));
			
			addMouseListener(new MouseAdapter() 
			{
	            public void mousePressed(MouseEvent e) 
	            {
	            	Vector2 mt = GameControl.CoordinatesToGrid(new Vector2(e.getX(), e.getY()));
	            	Debug.Log("Mouse Pressed @ " + mt.toString() + " Action: " + e.getButton(), this.getClass());
	            	
	            	boolean lol = false;
	            	
	            	
	            	if(e.getButton() == 1) //Left Click
	            	{
		            	for(int i = 0; i < gc.snakesObs.size(); i++)
		            	{
		            		for(int j = 0; j < gc.snakesObs.get(i).getBody().size(); j++)
		            		{
		            			Vector2 corpo_sexy = GameControl.CoordinatesToGrid( gc.snakesObs.get(i).getBody().get(j));
		            			
		            			if(corpo_sexy.equals(mt))
		            			{
		            				gc.selectedSnake = i;
		            				lol = true;
		            			}
		            		}
		            	}
	            	}
	            	else if(e.getButton() == 3)	//Right Click
	            	{
		            		gc.selectedSnake = -1;
	            	}
	            	
	            	if(!lol && gc.selectedSnake != -1)
	            	{
	            		gc.snakesObs.get(gc.selectedSnake).MoveTo(new Vector2(	gc.getGrid()[mt.getX()][mt.getY()].getPos().getX(),
	            															gc.getGrid()[mt.getX()][mt.getY()].getPos().getY()));
	            	}
	            	
	            	/*
	                gc.snake.MoveTo(new Vector2(gc.getGrid()[mt.getX()][mt.getY()].getPos().getX(), gc.getGrid()[mt.getX()][mt.getY()].getPos().getY()));
	                gc.snake2.MoveTo(new Vector2(gc.getGrid()[mt.getX()][mt.getY()].getPos().getX(), gc.getGrid()[mt.getX()][mt.getY()].getPos().getY()));
	                */
	            }
	        });
	    }

	    public void paintComponent(Graphics g) 
	    {
	        super.paintComponent(g);
	        g.setColor(new Color(140, 227, 155));
	        g.fillRect(0, 0, size.getX(), size.getY());
	        g.setColor(Color.BLACK);
	        

	        
	        for(int c = 0; c < gc.bonusList.size(); c++)
	        {
	        	gc.bonusList.get(c).Draw(g);
	        }
	        
	        
	        for(int s = 0; s < gc.snakesObs.size(); s++)
	        {
	        	gc.snakesObs.get(s).Draw(g);
	        }
	        
	        /*
	         * GRID DRAW
	        for(int i = 0; i < gc.getGrid().length; i++)
	        {
	        	for(int j = 0; j < gc.getGrid()[0].length; j++)
	        	{
	        		g.setColor(Color.LIGHT_GRAY);
	        		g.drawRect(i*GameControl.blockSize.getX(), j*GameControl.blockSize.getY(), GameControl.blockSize.getX(), GameControl.blockSize.getY());
	        	}
	        }
	        */

	        if(gc.selectedSnake != -1)
	        {
	        	g.drawString("Selected Snake: " + gc.selectedSnake, 10, 10);
	        	g.drawString("Size: " + gc.snakesObs.get(gc.selectedSnake).getBody().size(), 10, 40);
	        	g.drawString("Jumps: " + gc.snakesObs.get(gc.selectedSnake).getJumps(), 10, 60);
	        }
	        
	        //repaint();
	    }  
	}
}
